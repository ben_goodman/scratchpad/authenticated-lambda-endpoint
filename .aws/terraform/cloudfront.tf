module "cloudfront" {
    source  = "gitlab.com/ben_goodman/s3-website/aws"
    version = "2.2.0"

    org                                = var.resource_namespace
    project_name                       = var.project_name
    use_cloudfront_default_certificate = true
    default_cache_policy_id            = "658327ea-f89d-4fab-a63d-7e88639e58f6" # caching optimized
    default_response_headers_policy_id = "eaab4381-ed33-4a86-88ca-d9558dc6cd63" # CORS-with-preflight-and-SecurityHeadersPolicy

    additional_custom_origins = {
        "api" = {
            domain_name = trimsuffix(trimprefix(aws_lambda_function_url.test_origin_lambda_endpoint.function_url, "https://"), "/")
        }
    }

    ordered_cache_behaviors = {
        "api" = {
            path_pattern = "/api/*"
            viewer_protocol_policy = "https-only"
            cache_policy_id = "4135ea2d-6df8-44a3-9df3-4b5a84be39ad" # disable caching for API
            origin_request_policy_id = "b689b0a8-53d0-40ab-baf2-68738e2966ac" # Managed-AllViewerExceptHostHeader
            origin_request_lambda_arn = module.authentication_lambda.qualified_arn
        }
    }
}
