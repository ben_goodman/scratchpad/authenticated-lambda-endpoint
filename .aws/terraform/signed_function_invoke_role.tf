resource "aws_iam_policy" "lambda_invoke_function_policy" {
  name = "invoke-function-url-policy-${random_id.cd_function_suffix.hex}"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Action = [
            "lambda:InvokeFunctionUrl"
        ]
        Resource = [
            module.test_origin_lambda.arn
        ]
      }
    ]
  })
}


resource "aws_iam_role_policy_attachment" "lambda_invoke_signed_function_attachment" {
  role       = aws_iam_role.edge_lambda_iam_role.name
  policy_arn = aws_iam_policy.lambda_invoke_function_policy.arn
}