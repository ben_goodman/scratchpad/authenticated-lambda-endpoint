locals {
    test_origin_source_code_dir = "${path.module}/test_origin"
}

resource "null_resource" "test_origin_source_builder" {
    provisioner "local-exec" {
        working_dir = local.test_origin_source_code_dir
        command     = "make all"
    }
    triggers = {
        always_run = "${timestamp()}"
    }
}

data "null_data_source" "wait_for_test_origin_source_builder" {
    inputs = {
        lambda_exporter_id = "${null_resource.test_origin_source_builder.id}"
        source_file        = "${local.test_origin_source_code_dir}/dist/index.js"
    }
}

data "archive_file" "test_origin_build_payload" {
    depends_on       = [ data.null_data_source.wait_for_test_origin_source_builder ]
    type             = "zip"
    source_file      = data.null_data_source.wait_for_test_origin_source_builder.outputs.source_file
    output_file_mode = "0666"
    output_path      = "${local.test_origin_source_code_dir}/dist/index.js.zip"
}

module "test_origin_lambda" {
    source  = "gitlab.com/ben_goodman/lambda-function/aws"
    version = "2.0.1-b6fa9ae6-rc"

    org              = var.resource_namespace
    project_name     = var.project_name
    lambda_payload   = data.archive_file.test_origin_build_payload
    function_name    = "test-origin-${random_id.cd_function_suffix.hex}"
    function_handler = "index.handler"
    publish          = true
    memory_size      = 128
    # role             = aws_iam_role.dynamo_db_lambda_role
    # timeout          = 5
}

resource "aws_lambda_function_url" "test_origin_lambda_endpoint" {
    function_name      = module.test_origin_lambda.name
    authorization_type = "AWS_IAM"
}



