output "bucket_name" {
    value = module.cloudfront.bucket_name
}

output "cloudfront_id" {
    value = module.cloudfront.cloudfront_id
}

output "cloudfront_domain_name" {
    value = module.cloudfront.cloudfront_default_domain
}

output "lambda_function_url" {
    value = aws_lambda_function_url.test_origin_lambda_endpoint.function_url
}