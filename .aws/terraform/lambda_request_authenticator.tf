locals {
    authenticator_source_code_dir = "${path.module}/authenticator"
}

resource "null_resource" "authentication_handler_source_builder" {
    provisioner "local-exec" {
        working_dir = local.authenticator_source_code_dir
        command     = "make all"
    }
    triggers = {
        always_run = "${timestamp()}"
    }
}

data "null_data_source" "wait_for_authenticator_handler" {
    inputs = {
        lambda_exporter_id = "${null_resource.authentication_handler_source_builder.id}"
        source_file        = "${local.authenticator_source_code_dir}/dist/index.mjs"
    }
}

data "archive_file" "viewer_redirect_handler_build_payload" {
    depends_on       = [ data.null_data_source.wait_for_authenticator_handler ]
    type             = "zip"
    source_file      = data.null_data_source.wait_for_authenticator_handler.outputs.source_file
    output_file_mode = "0666"
    output_path      = "${local.authenticator_source_code_dir}/dist/index.mjs.zip"
}


module "authentication_lambda" {
    source  = "gitlab.com/ben_goodman/lambda-function/aws"
    version = "2.0.1"

    org              = var.resource_namespace
    project_name     = var.project_name
    lambda_payload   = data.archive_file.viewer_redirect_handler_build_payload
    function_name    = "redirecter-${random_id.cd_function_suffix.hex}"
    function_handler = "index.handler"
    publish          = true
    memory_size      = 128
    role             = aws_iam_role.edge_lambda_iam_role
}
