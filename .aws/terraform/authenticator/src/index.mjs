import { SignatureV4 } from '@aws-sdk/signature-v4';
import { fromNodeProviderChain } from '@aws-sdk/credential-providers';
import { HttpRequest } from "@aws-sdk/protocol-http";
const { createHash, createHmac } = await import('node:crypto');

function Sha256(secret) {
    return secret ? createHmac('sha256', secret) : createHash('sha256');
}

const credentialProvider = fromNodeProviderChain();
const credentials = await credentialProvider();

export const handler = async(event) => {

    const request = event.Records[0].cf.request;

    // remove the x-forwarded-for from the signature
    let headers = request.headers;
    delete headers['x-forwarded-for'];

    if (!request.origin.hasOwnProperty('custom'))
        throw("Unexpected origin type. Expected 'custom'. Got: " + JSON.stringify(request.origin));

    const uri = request.uri;
    const hostname = request.headers['host'][0].value;
    const region = hostname.split(".")[2];
    const path = uri + (request.querystring ? '?'+ request.querystring : '');
    const decodedBody = (request.body && request.body.data) ? Buffer.from(request.body.data, request.body.encoding) : undefined

    // [1] build the request to sign.
    // Its based on the incoming request from CloudFront.
    const req = new HttpRequest({
        hostname,
        path,
        body: decodedBody,
        method: request.method,
    });

    // (cloudfront header manipulation)
    for (const header of Object.values(headers)) {
        req.headers[header[0].key] = header[0].value;
    }

    // [2] sign the request with _this_ function's credentials
    // This works because this lambda function has the necessary
    // policy explicitly naming the secured function by its arn.
    const signer = new SignatureV4({
        credentials,
        region,
        service: 'lambda',
        sha256: Sha256,
    });

    const signedRequest = await signer.sign(req);

    // (more cloudfront header manipulation)
    for (const header in signedRequest.headers){
        request.headers[header.toLowerCase()] = [{
            key: header,
            value: signedRequest.headers[header].toString(),
        }];
    }

    // [3] return the signed request.
    // this will be passed to the origin (secure lambda function).
    return request;
}