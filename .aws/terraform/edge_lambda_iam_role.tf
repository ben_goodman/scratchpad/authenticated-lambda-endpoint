data "aws_iam_policy_document" "execution_role" {
    statement {
    effect = "Allow"

    principals {
        type        = "Service"
        identifiers = [
            "lambda.amazonaws.com",
            "edgelambda.amazonaws.com"
        ]
    }

        actions = ["sts:AssumeRole"]
    }
}


resource "aws_iam_role" "edge_lambda_iam_role" {
    name = "edge-lambda-exe-${random_id.cd_function_suffix.hex}"
    assume_role_policy = data.aws_iam_policy_document.execution_role.json
}

resource "aws_lambda_permission" "allow_cloudfront" {
  function_name = module.authentication_lambda.name
  statement_id  = "allow-cf-exec-${random_id.cd_function_suffix.hex}"
  action        = "lambda:GetFunction"
  principal     = "edgelambda.amazonaws.com"
}